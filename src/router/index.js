import Vue from "vue";
import VueRouter from "vue-router";


// 解决多次点击路由报错的问题。
const VueRouterPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(to) {
  return VueRouterPush.call(this, to).catch(err => err)
}


Vue.use(VueRouter);

const routes = [
  // 按需进行组件的懒加载。
  {
    path:"/",
    redirect:"/home"
  },
  {
    path: "/home",
    name: "home",
    component: () => import("../views/Home.vue"),
    meta: {
      isYes: true,
    }
  }, {
    path: "/type",
    name: "type",
    component: () => import("../views/Type.vue"),
    meta: {
      isYes: true,
    }
  }, {
    path: "/cart",
    name: "cart",
    component: () => import("../views/Cart.vue"),
    meta: {
      isYes: false,
    }
  }, {
    path: "/mine",
    name: "mine",
    component: () => import("../views/Mine.vue"),
    meta: {
      isYes: true,
    }
  },
  {
    path: "/login",
    name: "login",
    component: () => import("../views/Login.vue"),
    meta: {
      isYes: false,
    }
  },
  {
    path: "/reg",
    name: "reg",
    component: () => import("../views/Reg.vue"),
    meta: {
      isYes: false,
    }
  }
];

const router = new VueRouter({
  // mode: "history",
  // base: process.env.BASE_URL,
  routes
});

export default router;